package paint;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseAdapterImpl extends MouseAdapter {

    private PadDraw padDraw;

    public MouseAdapterImpl(PadDraw padDraw) {
        this.padDraw = padDraw;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        padDraw.setOldX(e.getX());
        padDraw.setOldY(e.getY());
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        padDraw.setCurrentX(e.getX());
        padDraw.setCurrentY(e.getY());
        if (padDraw.getGraphics2D() != null) {
            padDraw.getGraphics2D().drawLine(padDraw.getOldX(), padDraw.getOldY(), padDraw.getCurrentX(), padDraw.getCurrentY());
        }
        padDraw.repaint();
        padDraw.setOldX(padDraw.getCurrentX());
        padDraw.setOldY(padDraw.getCurrentY());
    }
}
