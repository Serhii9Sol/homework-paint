package paint;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {

        int bWidth = 90;
        int bHeight = 20;
        int fWidth = 600;
        int fHeight = 800;
        int pHeight = 50;

        PadDraw padDraw = new PadDraw();
        MouseAdapterImpl mouseAdapter = new MouseAdapterImpl(padDraw);
        ActionListenerImpl actionListener = new ActionListenerImpl(padDraw);

        JFrame frame = new JFrame("Paint");
        BorderLayout borderLayout = new BorderLayout();
        JPanel panel = new JPanel();

        JButton yellowButton = new JButton("YELLOW");
        JButton greenButton = new JButton("GREEN");
        JButton redButton = new JButton("RED");
        JButton blueButton = new JButton("BLUE");
        JButton blackButton = new JButton("BLACK");
        JButton clearButton = new JButton("CLEAR");

        Dimension bDimension = new Dimension(bWidth, bHeight);
        Dimension pDimension = new Dimension(fWidth, pHeight);

        JRadioButton jRadioButton1 = new JRadioButton("1 px");
        JRadioButton jRadioButton2 = new JRadioButton("3 px");
        JRadioButton jRadioButton3 = new JRadioButton("5 px");
        JRadioButton jRadioButton4 = new JRadioButton("10 px");
        ButtonGroup bg = new ButtonGroup();

        Painter painter = new Painter(frame, borderLayout, actionListener, mouseAdapter, padDraw, panel,
                bDimension, pDimension, clearButton, yellowButton, greenButton, redButton, blueButton, blackButton,
                jRadioButton1, jRadioButton2, jRadioButton3, jRadioButton4, bg, fHeight, fWidth);
        painter.start();
    }
}
