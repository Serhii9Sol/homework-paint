package paint;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionListenerImpl implements ActionListener {

    private PadDraw padDraw;

    public ActionListenerImpl(PadDraw padDraw) {
        this.padDraw = padDraw;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
        switch (action) {
            case "YELLOW" :
                padDraw.yellow();
                break;
            case "GREEN" :
                padDraw.green();
                break;
            case "RED" :
                padDraw.red();
                break;
            case "BLUE" :
                padDraw.blue();
                break;
            case "BLACK" :
                padDraw.black();
                break;
            case "CLEAR" :
                padDraw.clear();
                break;
            case "1 px" :
                padDraw.small();
                break;
            case "3 px" :
                padDraw.medium();
                break;
            case "5 px" :
                padDraw.large();
                break;
            case "10 px" :
                padDraw.xLarge();
                break;
            default :
                break;
        }
    }
}
