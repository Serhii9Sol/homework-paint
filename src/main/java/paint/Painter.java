package paint;

import javax.swing.*;
import java.awt.*;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Painter {

    private JFrame frame;
    private BorderLayout borderLayout;
    private Container content;
    private PadDraw padDraw;
    private ActionListenerImpl actionListener;
    private MouseAdapterImpl mouseAdapter;
    private JPanel panel;
    private Dimension bDimension;
    private Dimension pDimension;
    private JButton clearButton;
    private JButton yellowButton;
    private JButton greenButton;
    private JButton redButton;
    private JButton blueButton;
    private JButton blackButton;
    private JRadioButton rdbtnPx1;
    private JRadioButton rdbtnPx2;
    private JRadioButton rdbtnPx3;
    private JRadioButton rdbtnPx4;
    private ButtonGroup bg;
    private int fHeight;
    private int fWidth;

    public Painter(JFrame frame, BorderLayout borderLayout, ActionListenerImpl actionListener, MouseAdapterImpl mouseAdapter,
                   PadDraw padDraw, JPanel panel, Dimension bDimension, Dimension pDimension, JButton clearButton,
                   JButton yellowButton, JButton greenButton, JButton redButton, JButton blueButton, JButton blackButton,
                   JRadioButton rdbtnPx1, JRadioButton rdbtnPx2, JRadioButton rdbtnPx3, JRadioButton rdbtnPx4,
                   ButtonGroup bg, int fHeight, int fWidth) {

        this.frame = frame;
        this.borderLayout = borderLayout;
        this.actionListener = actionListener;
        this.mouseAdapter = mouseAdapter;
        this.padDraw = padDraw;
        this.panel = panel;
        this.bDimension = bDimension;
        this.pDimension = pDimension;
        this.clearButton = clearButton;
        this.yellowButton = yellowButton;
        this.greenButton = greenButton;
        this.redButton = redButton;
        this.blueButton = blueButton;
        this.blackButton = blackButton;
        this.rdbtnPx1 = rdbtnPx1;
        this.rdbtnPx2 = rdbtnPx2;
        this.rdbtnPx3 = rdbtnPx3;
        this.rdbtnPx4 = rdbtnPx4;
        this.bg = bg;
        this.fHeight = fHeight;
        this.fWidth = fWidth;
    }

    public void start() {
        content = frame.getContentPane();
        content.setLayout(borderLayout);
        content.add(padDraw, BorderLayout.CENTER);
        padDraw.setDoubleBuffered(false);

        panel.setPreferredSize(pDimension);

        clearButton.setForeground(Color.black);
        yellowButton.setForeground(Color.yellow);
        greenButton.setForeground(Color.green);
        redButton.setForeground(Color.red);
        blueButton.setForeground(Color.blue);
        blackButton.setForeground(Color.black);

        clearButton.setPreferredSize(bDimension);
        greenButton.setPreferredSize(bDimension);
        redButton.setPreferredSize(bDimension);
        yellowButton.setPreferredSize(bDimension);
        blueButton.setPreferredSize(bDimension);
        blackButton.setPreferredSize(bDimension);

        panel.add(blackButton);
        panel.add(blueButton);
        panel.add(redButton);
        panel.add(greenButton);
        panel.add(yellowButton);
        panel.add(clearButton);

        content.add(panel, BorderLayout.NORTH);

        panel.add(rdbtnPx1);
        panel.add(rdbtnPx2);
        panel.add(rdbtnPx3);
        panel.add(rdbtnPx4);

        bg.add(rdbtnPx1);
        bg.add(rdbtnPx2);
        bg.add(rdbtnPx3);
        bg.add(rdbtnPx4);
        rdbtnPx1.setSelected(true);

        frame.setSize(fWidth, fHeight);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setVisible(true);

        clearButton.addActionListener(actionListener);
        greenButton.addActionListener(actionListener);
        redButton.addActionListener(actionListener);
        yellowButton.addActionListener(actionListener);
        blueButton.addActionListener(actionListener);
        blackButton.addActionListener(actionListener);
        rdbtnPx1.addActionListener(actionListener);
        rdbtnPx2.addActionListener(actionListener);
        rdbtnPx3.addActionListener(actionListener);
        rdbtnPx4.addActionListener(actionListener);
        padDraw.addMouseListener(mouseAdapter);
        padDraw.addMouseMotionListener(mouseAdapter);
    }
}
