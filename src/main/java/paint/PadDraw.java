package paint;

import javax.swing.*;
import java.awt.*;

public class PadDraw extends JComponent {

    private Image image;
    private Graphics2D graphics2D;
    private int currentX, currentY, oldX, oldY;

    public PadDraw() {
    }

    public Image getImage() {
        return image;
    }

    public Graphics2D getGraphics2D() {
        return graphics2D;
    }

    public int getCurrentX() {
        return currentX;
    }

    public int getCurrentY() {
        return currentY;
    }

    public int getOldX() {
        return oldX;
    }

    public int getOldY() {
        return oldY;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setGraphics2D(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
    }

    public void setCurrentX(int currentX) {
        this.currentX = currentX;
    }

    public void setCurrentY(int currentY) {
        this.currentY = currentY;
    }

    public void setOldX(int oldX) {
        this.oldX = oldX;
    }

    public void setOldY(int oldY) {
        this.oldY = oldY;
    }

    @Override
    public void paintComponent(Graphics g) {
        if (image == null) {
            image = createImage(getSize().width, getSize().height);
            graphics2D = (Graphics2D) image.getGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            clear();

        }
        g.drawImage(image, 5, 5, null);
    }

    public void clear() {
        graphics2D.setPaint(Color.white);
        graphics2D.fillRect(0, 0, getSize().width, getSize().height);
        graphics2D.setPaint(Color.black);
        repaint();
    }

    public void red() {
        graphics2D.setPaint(Color.red);
        repaint();
    }

    public void black() {
        graphics2D.setPaint(Color.black);
        repaint();
    }

    public void yellow() {
        graphics2D.setPaint(Color.yellow);
        repaint();
    }

    public void blue() {
        graphics2D.setPaint(Color.blue);
        repaint();
    }

    public void green() {
        graphics2D.setPaint(Color.green);
        repaint();
    }

    public void small() {
        graphics2D.setStroke(new BasicStroke(1));
    }

    public void medium() {
        graphics2D.setStroke(new BasicStroke(3));
    }

    public void large() {
        graphics2D.setStroke(new BasicStroke(5));
    }

    public void xLarge() {
        graphics2D.setStroke(new BasicStroke(10));
    }
}
