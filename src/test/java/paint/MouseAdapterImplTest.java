package paint;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.awt.*;
import java.awt.event.MouseEvent;


class MouseAdapterImplTest {

    private final PadDraw padDraw = Mockito.mock(PadDraw.class);
    private final MouseEvent mouseEvent = Mockito.mock(MouseEvent.class);
    private final Graphics2D graphics2D = Mockito.mock(Graphics2D.class);
    private final int x = 50;
    private final int y = 60;
    private final int oldX = 49;
    private final int oldY = 59;
    private final int currentX = 51;
    private final int currentY = 61;
    private final MouseAdapterImpl cut = new MouseAdapterImpl(padDraw);


    @Test
    void mousePressedTest() {
        Mockito.when(mouseEvent.getX()).thenReturn(x);
        Mockito.when(mouseEvent.getY()).thenReturn(y);

        cut.mousePressed(mouseEvent);

        Mockito.verify(padDraw, Mockito.times(1)).setOldX(x);
        Mockito.verify(padDraw, Mockito.times(1)).setOldY(y);
    }

    @Test
    void mouseDraggedTest() {
        Mockito.when(mouseEvent.getX()).thenReturn(x);
        Mockito.when(mouseEvent.getY()).thenReturn(y);
        Mockito.when(padDraw.getGraphics2D()).thenReturn(graphics2D).thenReturn(graphics2D);
        Mockito.when(padDraw.getOldX()).thenReturn(oldX);
        Mockito.when(padDraw.getOldY()).thenReturn(oldY);
        Mockito.when(padDraw.getCurrentX()).thenReturn(currentX).thenReturn(currentX);
        Mockito.when(padDraw.getCurrentY()).thenReturn(currentY).thenReturn(currentY);

        cut.mouseDragged(mouseEvent);

        Mockito.verify(padDraw, Mockito.times(1)).setCurrentX(x);
        Mockito.verify(padDraw, Mockito.times(1)).setCurrentY(y);
        Mockito.verify(graphics2D, Mockito.times(1)).drawLine(oldX, oldY, currentX, currentY);
        Mockito.verify(padDraw, Mockito.times(1)).repaint();
        Mockito.verify(padDraw, Mockito.times(1)).setOldX(currentX);
        Mockito.verify(padDraw, Mockito.times(1)).setOldY(currentY);
    }
}