package paint;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import java.awt.*;
import static org.junit.jupiter.api.Assertions.*;

class PadDrawTest {
    private final Graphics2D graphics2D = Mockito.mock(Graphics2D.class);
    private final Image image = Mockito.mock(Image.class);
    private final PadDraw cut = new PadDraw();

    @BeforeEach
    void init() {
        cut.setGraphics2D(graphics2D);
    }

    @Test
    void clearTest() {
        cut.clear();

        Mockito.verify(graphics2D, Mockito.times(1)).setPaint(Color.white);
        Mockito.verify(graphics2D, Mockito.times(1)).fillRect(0, 0, 0, 0);
        Mockito.verify(graphics2D, Mockito.times(1)).setPaint(Color.black);
        assertDoesNotThrow((Executable) cut::repaint);
    }

    @Test
    void redTest() {
        cut.red();

        Mockito.verify(graphics2D, Mockito.times(1)).setPaint(Color.red);
        assertDoesNotThrow((Executable) cut::repaint);
    }

    @Test
    void blackTest() {
        cut.black();

        Mockito.verify(graphics2D, Mockito.times(1)).setPaint(Color.black);
        assertDoesNotThrow((Executable) cut::repaint);
    }

    @Test
    void blueTest() {
        cut.blue();

        Mockito.verify(graphics2D, Mockito.times(1)).setPaint(Color.blue);
        assertDoesNotThrow((Executable) cut::repaint);
    }

    @Test
    void yellowTest() {
        cut.yellow();

        Mockito.verify(graphics2D, Mockito.times(1)).setPaint(Color.yellow);
        assertDoesNotThrow((Executable) cut::repaint);
    }

    @Test
    void greenTest() {
        cut.green();

        Mockito.verify(graphics2D, Mockito.times(1)).setPaint(Color.green);
        assertDoesNotThrow((Executable) cut::repaint);
    }

    @Test
    void smallTest() {
        cut.small();

        Mockito.verify(graphics2D, Mockito.times(1)).setStroke(new BasicStroke(1));
    }

    @Test
    void mediumTest() {
        cut.medium();

        Mockito.verify(graphics2D, Mockito.times(1)).setStroke(new BasicStroke(3));
    }

    @Test
    void largeTest() {
        cut.large();

        Mockito.verify(graphics2D, Mockito.times(1)).setStroke(new BasicStroke(5));
    }

    @Test
    void xLargeTest() {
        cut.xLarge();

        Mockito.verify(graphics2D, Mockito.times(1)).setStroke(new BasicStroke(10));
    }

}