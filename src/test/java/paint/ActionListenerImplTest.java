package paint;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import java.awt.event.ActionEvent;
import java.util.List;

class ActionListenerImplTest {
    private final PadDraw padDraw = Mockito.mock(PadDraw.class);
    private final ActionEvent actionEvent = Mockito.mock(ActionEvent.class);
    private final ActionListenerImpl cut = new ActionListenerImpl(padDraw);

    static List<Arguments> actionPerformedTestArgs() {
        return List.of(
                Arguments.arguments(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, "YELLOW"),
                Arguments.arguments(0, 1, 0, 0, 0, 0, 0, 0, 0, 0, "GREEN"),
                Arguments.arguments(0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "RED"),
                Arguments.arguments(0, 0, 0, 1, 0, 0, 0, 0, 0, 0, "BLUE"),
                Arguments.arguments(0, 0, 0, 0, 1, 0, 0, 0, 0, 0, "BLACK"),
                Arguments.arguments(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, "CLEAR"),
                Arguments.arguments(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, "1 px"),
                Arguments.arguments(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, "3 px"),
                Arguments.arguments(0, 0, 0, 0, 0, 0, 0, 0, 1, 0, "5 px"),
                Arguments.arguments(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, "10 px"),
                Arguments.arguments(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "SomethingElse"));
    }

    @ParameterizedTest
    @MethodSource("actionPerformedTestArgs")
    void actionPerformedTest(int cYellow, int cGreen, int cRed, int cBlue, int cBlack, int cClear, int cS,
                             int cM, int cL, int cXL, String actionCommand) {
        Mockito.when(actionEvent.getActionCommand()).thenReturn(actionCommand);

        cut.actionPerformed(actionEvent);

        Mockito.verify(padDraw, Mockito.times(cYellow)).yellow();
        Mockito.verify(padDraw, Mockito.times(cGreen)).green();
        Mockito.verify(padDraw, Mockito.times(cRed)).red();
        Mockito.verify(padDraw, Mockito.times(cBlue)).blue();
        Mockito.verify(padDraw, Mockito.times(cBlack)).black();
        Mockito.verify(padDraw, Mockito.times(cClear)).clear();
        Mockito.verify(padDraw, Mockito.times(cS)).small();
        Mockito.verify(padDraw, Mockito.times(cM)).medium();
        Mockito.verify(padDraw, Mockito.times(cL)).large();
        Mockito.verify(padDraw, Mockito.times(cXL)).xLarge();
    }
}